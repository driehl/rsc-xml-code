
WITH RSC_XML (SalesPersonID, SalesOrderID, SalesYear)  
AS  
-- Define the CTE query.  
(  
    SELECT SalesPersonID, SalesOrderID, YEAR(OrderDate) AS SalesYear  
    FROM Sales.SalesOrderHeader  
    WHERE SalesPersonID IS NOT NULL  
)  
-- Define the outer query referencing the CTE name.  
SELECT SalesPersonID, COUNT(SalesOrderID) AS TotalSales, SalesYear  
FROM Sales_CTE  
GROUP BY SalesYear, SalesPersonID  
ORDER BY SalesPersonID, SalesYear; 


* tenant scode
* property scode
* first name
* last name
* unit number

select top 10 t.hmy AS '@ID', t.scode as tenant_code, p.scode as prop_key, t.sfirst_name as firstname, t.SLASTNAME as last_name, t.sunitcode as unit
from tenant t 
inner join property p on p.hmy = t.HPROPERTY
FOR XML PATH('link')


<link v="1" db_name="Yardi">
  <household db_key="t1234" prop_key="p567" first_name="Nate" last_name="Chrysler" unit="w703"/>
</link>

select top 1 'household db_name="Yardi" db_key="' + cast(t.HMYPERSON as varchar) + '" tenant_code="' + cast(t.scode as varchar) + '" prop_key="' + cast(p.scode as varchar) + '" first_name="' + cast(t.sfirstname as varchar) + '" last_name="' + cast(t.SLASTNAME as varchar) + '" unit="' +  t.sunitcode + '"' as 'Data'
from tenant t 
inner join property p on p.hmy = t.HPROPERTY
FOR XML PATH('link'), ELEMENTS

select '<link v="1">' +
cast((select 'household db_name="Yardi"' as 'db'
,'db_key="' + rtrim(ltrim(cast(t.HMYPERSON as varchar))) + '"' as 'db_key'
,'tenant_code="' + rtrim(ltrim(cast(t.scode as varchar))) + '"' as 'tenant_code'
,'prop_key="' + rtrim(ltrim(cast(p.scode as varchar))) + '"' as 'prop_key'
,'first_name="' + rtrim(ltrim(cast(t.sfirstname as varchar))) + '"' as 'first_name'
,'last_name="' + rtrim(ltrim(cast(t.SLASTNAME as varchar))) + '"' as 'last_name'
,'unit="' +  rtrim(ltrim(cast(t.sunitcode as varchar))) + '"' as 'unit'
from tenant t 
inner join property p on p.hmy = t.HPROPERTY
where t.HMYPERSON = 2164
FOR XML PATH(''))  AS VARCHAR(MAX)) + '</link>' AS XmlData

select top 10 1 AS '@v', 'Yardi' as '@db_name', t.scode as 'household/@db_key', rtrim(ltrim(p.scode)) as 'household/@prop_name', t.sfirstname as 'household/@first_name', t.SLASTNAME as 'household/@last_name', rtrim(ltrim(t.sunitcode)) as 'household/@unit'
from tenant t 
inner join property p on p.hmy = t.HPROPERTY
where t.HMYPERSON = 2164
FOR XML PATH('link')

select TOP 100  1  AS '@v'
, 'Yardi' as '@db_name'
, t.scode as 'household/@db_key'
, rtrim(ltrim(p.scode)) as 'household/@prop_name'
, t.sfirstname as 'household/@first_name'
, t.SLASTNAME as 'household/@last_name'
, rtrim(ltrim(t.sunitcode)) as 'household/@unit'
, (select max(IMEMBERNUM) from dbo.[hmFamily] hmf where hmf.hSumm = (select top 1 hs.hVerify from HMSUMM hs where hs.htenant = t.hmyperson ORDER BY hs.dteffective DESC)) as 'household/@num_members'
, (select COUNT(hmy) from dbo.[hmFamily] hmf where hmf.hSumm = (select top 1 hs.hVerify from HMSUMM hs where hs.htenant = t.hmyperson ORDER BY hs.dteffective DESC) and  hmf.IAGE >= 18) as 'household/@num_adults'
, (select COUNT(hmy) from dbo.[hmFamily] hmf where hmf.hSumm = (select top 1 hs.hVerify from HMSUMM hs where hs.htenant = t.hmyperson ORDER BY hs.dteffective DESC) and  hmf.sRelation = 'S') as 'household/@num_couples'
from tenant t 
inner join property p on p.hmy = t.HPROPERTY
where t.ISTATUS = 0 
ORDER BY t.dtCreated DESC 
FOR XML PATH('link')