 drop table #XML
 
 create table #XML(
 hmy int identity (1,1) ,
 hMenu nvarchar(500),
 sCaption  nvarchar(500),
 sLinkCaption   nvarchar(500),
 sLink  nvarchar(500),
 iOrder  nvarchar(500),
 iLevel nvarchar(500),
 iCol nvarchar(500),
 xmldata xml)

 DECLARE @Order NUMERIC 

 SET @Order = 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  'Home', '', '"JAVASCRIPT:Home()"',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  'Filter', '', '"JAVASCRIPT:Filter()"',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  'Daily Activity', '', '"../iData.asp?WCI=begin&Action=Filter&select=filters\\Daily_Activity_ft.txt&redirect=Reports/Daily_Activity.asp" target="filter"',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  'Quick Menu', '', '"../Menus/menu_quick.asp?hGroup=&sMenuSet=CompSr" target="filter"',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  '"Guests"', '', '""',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  '-"Find Person"', '', '"../pages/PersonSearchPage.aspx"',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  '"Residential"', '', '""',  @Order, 0, 0) 
 SET @Order = @Order + 1 
 INSERT INTO #XML (hMenu, sCaption, sLinkCaption, sLink, iOrder, iLevel, iCol) VALUES (
'@MenuTitleId',  '-"Find Person"', '', '"../pages/PersonSearchPage.aspx"',  @Order, 0, 0) 


select * from #XML




<Nate ID="1">
  <scaption>Home</scaption>
  <slink>"JAVASCRIPT:Home()"</slink>
  <hmenu>@MenuTitleId</hmenu>
  <iorder>1</iorder>
  <ilevel>0</ilevel>
  <icol>0</icol>
</Nate>

--CREATE MASTER KEY ENCRYPTION BY PASSWORD = 'pGFD4bb925DGvbd2439587y'

--CREATE CERTIFICATE RSCXML037   
--   ENCRYPTION BY PASSWORD = 'pGFD4bb925DGvbd2439587y'  
--   WITH SUBJECT = 'RSC Records',   
--   EXPIRY_DATE = '20201031';  

--ALTER CERTIFICATE RSCXML037
--WITH PRIVATE KEY (DECRYPTION BY PASSWORD = 'pGFD4bb925DGvbd2439587y');

--CREATE SYMMETRIC KEY RSC_Key_01   
--WITH ALGORITHM = AES_256  
--ENCRYPTION BY CERTIFICATE RSCXML037;  
  
--OPEN SYMMETRIC KEY RSC_Key_01  
--   DECRYPTION BY CERTIFICATE RSCXML037;  
  
--	select EncryptByKey(Key_GUID('RSC_Key_01'), scaption)
--	from #XML
--	where hmy = 1 
--GO  

--update #XML 
--set xmldata = (
SELECT
	hmy AS '@ID',
	 scaption as scaption, 
     slink as slink,
     hmenu as hmenu, 
     iorder as iorder,
     ilevel as ilevel,
     icol as icol
from #XML
--where hmy = 1 
FOR XML PATH('RSC')
--) where  hmy = 1 
--GO

<Nate ID="1"><scaption>Home</scaption><slink>"JAVASCRIPT:Home()"</slink><hmenu>@MenuTitleId</hmenu><iorder>1</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="2"><scaption>Filter</scaption><slink>"JAVASCRIPT:Filter()"</slink><hmenu>@MenuTitleId</hmenu><iorder>2</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="3"><scaption>Daily Activity</scaption><slink>"../iData.asp?WCI=begin&amp;Action=Filter&amp;select=filters\\Daily_Activity_ft.txt&amp;redirect=Reports/Daily_Activity.asp" target="filter"</slink><hmenu>@MenuTitleId</hmenu><iorder>3</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="4"><scaption>Quick Menu</scaption><slink>"../Menus/menu_quick.asp?hGroup=&amp;sMenuSet=CompSr" target="filter"</slink><hmenu>@MenuTitleId</hmenu><iorder>4</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="5"><scaption>"Guests"</scaption><slink>""</slink><hmenu>@MenuTitleId</hmenu><iorder>5</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="6"><scaption>-"Find Person"</scaption><slink>"../pages/PersonSearchPage.aspx"</slink><hmenu>@MenuTitleId</hmenu><iorder>6</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="7"><scaption>"Residential"</scaption><slink>""</slink><hmenu>@MenuTitleId</hmenu><iorder>7</iorder><ilevel>0</ilevel><icol>0</icol></Nate><Nate ID="8"><scaption>-"Find Person"</scaption><slink>"../pages/PersonSearchPage.aspx"</slink><hmenu>@MenuTitleId</hmenu><iorder>8</iorder><ilevel>0</ilevel><icol>0</icol></Nate>

SELECT 
   xmldata.value('declare namespace ID="1"; (/ns:Nate/ns:scaption)[1]','varchar(50)') AS scaption,
   xmldata.value('declare namespace ID="1"; (/ns:Nate/ns:slink)[1]','varchar(50)') AS slink,
   xmldata.value('declare namespace ID="1"; (/ns:Nate/ns:hmenu)[1]','bit') AS hmenu,
   xmldata.value('declare namespace ID="1"; (/ns:Nate/ns:iorder)[1]','int') AS iorder
FROM #XML

GO 

<RSC ID="1"><scaption>Home</scaption><slink>"JAVASCRIPT:Home()"</slink><hmenu>@MenuTitleId</hmenu><iorder>1</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="2"><scaption>Filter</scaption><slink>"JAVASCRIPT:Filter()"</slink><hmenu>@MenuTitleId</hmenu><iorder>2</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="3"><scaption>Daily Activity</scaption><slink>"../iData.asp?WCI=begin&amp;Action=Filter&amp;select=filters\\Daily_Activity_ft.txt&amp;redirect=Reports/Daily_Activity.asp" target="filter"</slink><hmenu>@MenuTitleId</hmenu><iorder>3</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="4"><scaption>Quick Menu</scaption><slink>"../Menus/menu_quick.asp?hGroup=&amp;sMenuSet=CompSr" target="filter"</slink><hmenu>@MenuTitleId</hmenu><iorder>4</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="5"><scaption>"Guests"</scaption><slink>""</slink><hmenu>@MenuTitleId</hmenu><iorder>5</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="6"><scaption>-"Find Person"</scaption><slink>"../pages/PersonSearchPage.aspx"</slink><hmenu>@MenuTitleId</hmenu><iorder>6</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="7"><scaption>"Residential"</scaption><slink>""</slink><hmenu>@MenuTitleId</hmenu><iorder>7</iorder><ilevel>0</ilevel><icol>0</icol></RSC><RSC ID="8"><scaption>-"Find Person"</scaption><slink>"../pages/PersonSearchPage.aspx"</slink><hmenu>@MenuTitleId</hmenu><iorder>8</iorder><ilevel>0</ilevel><icol>0</icol></RSC>

