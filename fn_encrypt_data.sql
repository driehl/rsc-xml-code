create function fn_encrypt_data (@value varchar(3990))
returns varchar(8000)
as
BEGIN

DECLARE @ReturnValue varbinary(8000)
DECLARE @ReturnHex varchar(8000)

DECLARE @PassphraseEnteredByUser nvarchar(128);  
SET @PassphraseEnteredByUser   
    = 'A little learning is a dangerous thing!';  

select @ReturnValue = EncryptByPassPhrase(@PassphraseEnteredByUser, cast(@value as varchar))

select @ReturnHex = master.dbo.fn_varbintohexstr(@ReturnValue)

return @ReturnHex
END 
go
